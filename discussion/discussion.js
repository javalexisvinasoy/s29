// MongoDB - Query Operators and Field Projection


db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}
	]);


// Comparison Query Operators

// $gt operator - it matches the values that are greater than a specified value.

// $gte operator - it matches the values that are greater than or equal to a specified value.

/*
	Syntax:
		$gt
			db.collectionName.find({"field": {$gt: value}})
		$gte
			db.collectionName.find({"field": {$gte: value}})	
*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$gte: 50
	}
});


// $lt operator - it matches the values that are less than a specified value.
// $lte - it matches the values that are less than or equal to a specified value.

/*
	Syntax:
		$lt
			db.collectionName.find({"field": {$lt: value}})
		$lte
			db.collectionName.find({"field": {$lte: value}})	
*/

db.inventory.find({
	"stocks": {
		$lt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lte: 50
	}
});


// $ne (not equal) operator - it matches all values that are not equal to a specified value

/*
	Syntax:
		db.collectionName.find({"field": {$ne: value}})
*/ 

db.inventory.find({
	"stocks": {
		$ne: 50
	}
});


// $eq operator - it matches values that are equal to a specified value

/*
	Syntax:
		db.collectionName.find({"field": {$eq: value}})
*/ 

db.inventory.find({
	"stocks": {
		$eq: 50
	}
});


// $in operator - it matches any of the values specified in an array

/*
	Syntax:
		db.collectionName.find({"field": {$in: [value1, value2]}})
*/

db.inventory.find({
	"price": {
		$in: [5000, 10000]
	}
});


// $nin operator - it matches non of the values specified in an array

/*
	Syntax:
		db.collectionName.find({"field": {$nin: [value1, value2]}})
*/

db.inventory.find({
	"price": {
		$nin: [5000, 10000]
	}
});


// To create a range condition

db.inventory.find({
	"price": {$gt: 2000} && {$lt: 4000}
})


/*
	Mini-Activity:
		1. In the inventory collection, return all the documents that have the price equal or less than 4000.
		2. In the inventory collection, return all the documents that have stocks of 50 and 100
*/


db.inventory.find({
	"stocks": {
		$lte: 4000
	}
});

db.inventory.find({
	"stocks": {
		$in: [50, 100]
	}
});



// Logical Query Operators

/*
	$or operator 
		- it joins query clauses with a logical OR operator and returns all documents  that matches the conditions of their clauses.

	Syntax:
		db.collectionName.find({
			$or: [
				{"fieldA": "valueA"},
				{"fieldB": "valueB"}
			]
		})	
*/

db.inventory.find({
	$or: [
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House"}
	]
});


db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": {
			$lte: 5000
		}}
	]
});

/*
	$and operator
		- it joins the query clauses with a logical AND operator and returns all documents that matches the conditions of both clauses.

	Syntax:
		db.collectionName.find({
			$and: [
				{"fieldA": "valueA"},
				{"fieldB": "valueB"}
			]
		})		
*/

db.inventory.find({
	$and: [
		{"stocks": {
			$ne: 50
		}},
		{"price": {
			$ne: 5000
		}}
	]
});

// 
db.inventory.find({

		"stocks": {
			$ne: 50
		},
		"price": {
			$ne: 5000
		}
});


// Field Projection
// Inclusion - it matches the document according to the given criteria and returns included fields.

// 1- if include

/*
	Syntax:
		db.collectionName.find({criteria}, {"field": 1})
*/

db.inventory.find({
	"publisher": "JS Publishing House"
	},
	{
		"name": 1,
		"author": 1,
		"price": 1
	}
)


// Exclusion - it matches documents with the given criteria and returns fields that were not excluded

// 0- if exclude

/*
	Syntax:
		db.collectionName.find({criteria}, {"field": 1})
*/

db.inventory.find(
		{
			"author": "Noah Jimenez"
		},
		{
			"price": 0,
			"stocks": 0
		}
	)

// IMPORTANT NOTE: WE CAN'T COMBINE INCLUSION AND EXCLUSION EXCEPT WHEN HIDING THE _id

db.inventory.find(
		{
			"price": {
				$lte: 5000
			}
		},
		{
			"_id": 0,
			"name": 1,
			"author": 1
		}
	)


// Evaluation Query Operator

/*
	$regex operator (regular expression)
		- it selects documents where values matches a specified regular expression

	Syntax:
		db.collectionName.find({"field": {$regex: 'patter', $options: 'optionValue'}})	
*/

// Case Sensitive

db.inventory.find(
		{"author": 
			{
				$regex: 'M'
			}	
		}
	);

// Case insensitive

db.inventory.find(
		{"author": 
			{
				$regex: 'M',
				$options: '$i' 
			}	
		}
	);

// Multiple letters in the patter

db.inventory.find(
		{"name": 
			{
				$regex: 'dev',
				$options: '$i'
			}	
		}
	);


/*
	Mini Activity:
		- in the inventory collections, find a "name" of a book/s with letters "java" in it and has a "price" of greater than or equal to 5000.
			-- use $and, $regex and $gte operators
*/

db.inventory.find({
	$and: [ {"name": {
		$regex: 'java',
		$options: '$i'
	}},
	{
		"price": {
			$gte: 5000
		}
	}
	]
});

// Can we use query operators in other CRUD operations like updateOne, updateMany, deleteOne, deleteMany? YES!!!

/*
	updateOne({criteria/query}, {$set})
	updateMany({criteria/query}, {$set})
	deleteOne({criteria/query})
	deleteMany({criteria/query})
*/